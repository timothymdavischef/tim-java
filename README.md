tim-java Cookbook
=================

This cookbook is a simple wrapper so that I can specify some attributes at the cookbook level instead of overriding them for a role or in a node.

Requirements
------------
See OpsCode's java cookbook for requirements.

e.g.
#### packages
- `java` - tim-java needs java and all of it's dependencies.

Attributes
----------

This is just a wrapper for the default OpsCode java cookbook. It should have all of the same attributes as the original.

Usage
-----
#### tim-java::default

Just include `tim-java` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[tim-java]"
  ]
}
```

License and Authors
-------------------
Authors: Tim Davis
