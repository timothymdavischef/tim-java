name             'tim-java'
maintainer       'Stitchy, LLC'
maintainer_email 'tim@stitchy.org'
license          'All rights reserved'
description      'Installs/Configures tim-java'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

depends          'java'
